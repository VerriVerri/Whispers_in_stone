using TMPro;
using UnityEngine;

public class Espectro : MonoBehaviour
{
    public float xAxisRange = 5f;
    public float yAxisRange = 3f;
    public float amplitude = 0.5f;
    public float frequency = 1f;
    public float speed = 1f;
    private bool isFacingRight = true; // Indicador de la dirección del enemigo

    private Vector3 startPos;
    private Vector3 targetPos;

    void Start()
    {
        startPos = transform.position;
        targetPos = GetRandomPosition();
    }

    void Update()
    {
        Vector3 movementDirection = (targetPos - transform.position).normalized;
        if (movementDirection.x < 0 && isFacingRight)
        {
            FlipSprite();
        }
        // Voltea el sprite si la dirección del movimiento cambia a la derecha
        else if (movementDirection.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        // Move towards the target position
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        // If we've reached the target position, get a new one
        if (Vector3.Distance(transform.position, targetPos) < 0.1f)
        {
            targetPos = GetRandomPosition();
        }

        // Calculate the sine wave offset based on time
        float sineWaveOffset = amplitude * Mathf.Sin(2 * Mathf.PI * frequency * Time.time);

        // Apply the sine wave offset to the y-axis position
        Vector3 newPos = transform.position;
        newPos.y = startPos.y + sineWaveOffset;

        // Restrict the movement to the defined area
        newPos.x = Mathf.Clamp(newPos.x, startPos.x - xAxisRange, startPos.x + xAxisRange);
        newPos.z = Mathf.Clamp(newPos.z, startPos.z - yAxisRange, startPos.z + yAxisRange);

        // Move towards the new position with a logarithmic deceleration
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * speed / Vector3.Distance(transform.position, newPos));
    }

    private Vector3 GetRandomPosition()
    {
        return new Vector3(Random.Range(startPos.x - xAxisRange, startPos.x + xAxisRange), startPos.y, Random.Range(startPos.z - yAxisRange, startPos.z + yAxisRange));
    }
    private void FlipSprite()
    {
        // Voltea el sprite horizontalmente
        isFacingRight = !isFacingRight;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*public float movementSpeed = 5f; // Velocidad de movimiento del enemigo
    public float xAxisRange = 5f; // Área en el eje X
    public float yAxisRange = 3f; // Área en el eje Y

    private float minX, maxX, minY, maxY;
    private Vector3 targetPosition;
    private bool isFacingRight = true; // Indicador de la dirección del enemigo








    private void Start()
    {
        // Calcula los límites del área en los ejes X e Y
        minX = transform.position.x - xAxisRange;
        maxX = transform.position.x + xAxisRange;
        minY = transform.position.y - yAxisRange;
        maxY = transform.position.y + yAxisRange;

        // Establece la posición inicial del objetivo dentro del área permitida
        targetPosition = GetRandomPosition();
    }

    private void Update()
    {
        // Calcula la dirección del movimiento
        Vector3 movementDirection = (targetPosition - transform.position).normalized;

        // Voltea el sprite si la dirección del movimiento cambia a la izquierda
        if (movementDirection.x < 0 && isFacingRight)
        {
            FlipSprite();
        }
        // Voltea el sprite si la dirección del movimiento cambia a la derecha
        else if (movementDirection.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }

        // Mueve al enemigo hacia el objetivo
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, movementSpeed * Time.deltaTime);

        // Si el enemigo alcanza el objetivo, selecciona una nueva posición de destino dentro del área
        if (transform.position == targetPosition)
        {
            targetPosition = GetRandomPosition();
        }
    }

    private Vector3 GetRandomPosition()
    {
        // Genera una nueva posición aleatoria dentro del área permitida
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);
        return new Vector3(randomX, randomY, 0f);
    }

    private void FlipSprite()
    {
        // Voltea el sprite horizontalmente
        isFacingRight = !isFacingRight;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    */


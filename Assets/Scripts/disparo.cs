using System.Collections.Generic;
using UnityEngine;

public class disparo : MonoBehaviour
{
    public GameObject projectilePrefab; // Prefab del proyectil
    public Transform firePoint; // Punto de origen del disparo
    public int poolSize = 10; // Tamaño de la piscina de proyectiles
    public float shootingRange = 5f; // Rango de disparo
    public float shootingCooldown = 2f; // Tiempo de espera entre disparos

    private Transform player; // Referencia al transform del jugador
    private List<GameObject> projectilePool; // Piscina de proyectiles disponibles
    private float shootingTimer = 0f; // Temporizador para el tiempo de espera entre disparos
    private int nextProjectileIndex = 0; // Índice para obtener el siguiente proyectil de la piscina

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("PJ").transform; // Buscar el objeto del jugador por etiqueta

        // Inicializar la piscina de proyectiles
        projectilePool = new List<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject projectile = Instantiate(projectilePrefab);
            projectile.SetActive(false);
            projectilePool.Add(projectile);
        }
    }

    private void Update()
    {
        // Calcular la distancia al jugador
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        // Si el jugador está dentro del rango de disparo y ha pasado el tiempo de espera entre disparos
        if (distanceToPlayer <= shootingRange && shootingTimer <= 0f)
        {
            Shoot();
            shootingTimer = shootingCooldown; // Reiniciar el temporizador de espera
        }

        // Actualizar el temporizador de espera
        if (shootingTimer > 0f)
        {
            shootingTimer -= Time.deltaTime;
        }
    }

    private void Shoot()
    {
        // Obtener el siguiente proyectil disponible en la piscina
        GameObject projectile = projectilePool[nextProjectileIndex];
        nextProjectileIndex = (nextProjectileIndex + 1) % poolSize;

        // Configurar la posición y rotación del proyectil
        projectile.transform.position = firePoint.position;
        projectile.transform.rotation = firePoint.rotation;

        // Activar el proyectil y dispararlo
        projectile.SetActive(true);
      //  projectile.GetComponent<bala>().Shoot();
    }
}

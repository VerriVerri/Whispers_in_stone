using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;

public class wheely : MonoBehaviour
{
    public float moveSpeed = 3f;
    public float dashSpeed = 8f;

    private Transform player;
    private bool isFacingRight = true;

    private bool detecta;
  //  private bool rueda;
 //   private bool muere;
    private bool mover;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("PJ").transform;
    }

    private void Update()
    {
        MoveRandomly();

        if (DetectPlayer())
        {
            DashTowardsPlayer();
        }
    }

    private void MoveRandomly()
    {
        
        mover=true;
        transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
    }

    private bool DetectPlayer()
    {        //detectar=true;

        float distanceToPlayer = Vector2.Distance(transform.position, player.position);
        return distanceToPlayer < 5f;
    }

    private void DashTowardsPlayer()
    {
        //rueda=true;
        // Calcula la dirección hacia el jugador
        Vector2 direction = (player.position - transform.position).normalized;

        // Cambia la escala del enemigo para reflejar el sprite horizontalmente si mira hacia la izquierda
        if (direction.x < 0 && isFacingRight)
        {
            FlipSprite();
        }
        // Restaura la escala del enemigo si mira hacia la derecha
        else if (direction.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }

        // Mueve al enemigo a alta velocidad hacia el jugador
        // Puedes usar transform.Translate() o Rigidbody2D para mover al enemigo
        transform.Translate(direction * dashSpeed * Time.deltaTime);
    }

    private void FlipSprite()
    {
        isFacingRight = !isFacingRight;

        // Obtiene la escala actual del enemigo
        Vector3 scale = transform.localScale;
        // Invierte el valor del eje X para reflejar horizontalmente el sprite
        scale.x *= -1;
        // Aplica la nueva escala al enemigo
        transform.localScale = scale;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Destruye al enemigo cuando colisiona con el jugador
            Destroy(gameObject);
        }
    }
}

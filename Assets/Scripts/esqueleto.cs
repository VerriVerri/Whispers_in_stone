using UnityEngine;

public class esqueleto : MonoBehaviour
{
    public float movementSpeed = 3f; // Velocidad de movimiento del enemigo
    public float detectionRange = 10f; // Rango de detección del jugador
    public float maintainDistance = 2f; // Distancia a mantener desde el jugador

    public Transform player; // Referencia al transform del jugador

    private bool isPlayerDetected = false; // Indicador de detección del jugador

    private SpriteRenderer spriteRenderer; // Referencia al componente SpriteRenderer

    private void Start()
    {
        // Obtener la referencia al componente SpriteRenderer
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        // Calcular la distancia al jugador
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        if (isPlayerDetected)
        {
            // Si el jugador está dentro del rango de detección, moverse hacia él manteniendo la distancia
            if (distanceToPlayer > maintainDistance)
            {
                MoveTowardsPlayer();
            }
        }
        else
        {
            // Si no se ha detectado al jugador, esperar a la detección
            if (distanceToPlayer <= detectionRange)
            {
                isPlayerDetected = true;
            }
        }

        // Actualizar la orientación del sprite según la dirección del movimiento
        UpdateSpriteOrientation();
    }

    private void MoveTowardsPlayer()
    {
        // Calcular la dirección hacia el jugador
        Vector3 direction = (player.position - transform.position).normalized;

        // Calcular la posición objetivo manteniendo la distancia específica
        Vector3 targetPosition = player.position - direction * maintainDistance;

        // Mover al enemigo hacia la posición objetivo
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, movementSpeed * Time.deltaTime);
    }

    private void UpdateSpriteOrientation()
    {
        // Obtener la escala local actual
        Vector3 localScale = transform.localScale;

        // Si el enemigo se está moviendo hacia la izquierda, reflejar horizontalmente el sprite
        if (player.position.x < transform.position.x)
        {
            localScale.x = -Mathf.Abs(localScale.x);
        }
        // Si el enemigo se está moviendo hacia la derecha, mantener la orientación normal del sprite
        else if (player.position.x > transform.position.x)
        {
            localScale.x = Mathf.Abs(localScale.x);
        }

        // Asignar la escala local actualizada al transform
        transform.localScale = localScale;
    }
}

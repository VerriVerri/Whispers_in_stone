using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public string checkpointName;

    public bool checkpointActive;

    public CheckpointManager myCheckpointManager;

    private void Start()
    {
        myCheckpointManager = FindObjectOfType<CheckpointManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        myCheckpointManager.ActivateCheckpoint(this);
        //activo un sprite
    }
}


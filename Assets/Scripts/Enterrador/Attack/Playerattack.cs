using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerattack : MonoBehaviour
{
    public PlayerInput playerinput;
    public PlayerMovement playerMovement;
    public bool isAttacking = false;
    public float resetAttackTimer = 0.2f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (true)
        {
            transform.localPosition = new Vector3(0.88f, 0f, 0f);
        }
        else
        {
            transform.localPosition = new Vector3(-0.88f, 0f, 0f);
        }


        if (!isAttacking && Input.GetButtonDown("Attack"))
        {
            Debug.Log("Successful Attack");
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {
        playerMovement.animatorPlayer.SetTrigger("Attack");
        yield return new WaitForSeconds(resetAttackTimer);
        Debug.Log("Works?");
        isAttacking = false;

    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    //public GameObject
    public List<Checkpoint> checkpoints;
    public PlayerMovement myPlayer;
    public Rigidbody2D myRb;
    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        myRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        checkpoints = FindObjectsOfType<Checkpoint>().ToList<Checkpoint>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawnOnCheckpoint()
    {
        for (int i = 0; i < checkpoints.Count; i++)
        {
            if (checkpoints[i].checkpointActive)
            {
                myPlayer.transform.position = checkpoints[i].transform.position;

            }
        }
    }
    public void ActivateCheckpoint(Checkpoint checkpointToActivate)
    {
        for (int i = 0; i < checkpoints.Count; i++)
        {
            if (checkpoints[i] == checkpointToActivate)
            {
                checkpoints[i].checkpointActive = true;
            }
            else
            {
                checkpoints[i].checkpointActive = false;
            }
        }
    }
}

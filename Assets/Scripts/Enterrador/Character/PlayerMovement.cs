using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using UnityEngine;
using Unity.VisualScripting;

public class PlayerMovement : MonoBehaviour
{
    [Header("Components")]
    private PlayerInput playerInput; // Reference to the PlayerInput component
    [SerializeField] private Rigidbody2D rb; // Reference to the Rigidbody2D component                                            
    [SerializeField] public Animator animatorPlayer;
    public CheckpointManager myCheckpointManager;

    [Header("Movement")]
    [SerializeField] private float movementAcceleration; // The rate at which the player's velocity increases when moving
    [SerializeField] private float maxSpeed; // The maximum horizontal speed of the player
    [SerializeField] public float linearDrag; // The amount of drag applied to the player's movement when not moving
    [SerializeField] private float jumpThrust; // The amount of force necessary to 
    public float dragAcceleration;

    [Header("Data")]
    public Vector2 currentVelocity;
    

    [SerializeField] private float hDirection; // The current horizontal direction of the player's movement
    public float jump = 1;
    public Vector2 trueDirection;
    public float multiplier = 1;
    public float placeholderfloat;
    public bool isFacingRight;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // Get the Rigidbody2D component
        playerInput = GetComponent<PlayerInput>(); // Get the PlayerInput component
        Vector2 jump = new (0, jumpThrust);
        myCheckpointManager = FindObjectOfType<CheckpointManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInput.isAlive)
        {
            hDirection = GetInput().x; // Update the horizontal direction based on the user input
            animatorPlayer.SetBool("isGrounded", playerInput.isGrounded);
            if (playerInput.isGrounded)
            {
                jump = 1;
            }
            if (!playerInput.isAttacking)
            {
                hDirection = GetInput().x; // Update the horizontal direction based on the user input
            }
            else
            {
                hDirection = 0;
            }
            animatorPlayer.SetFloat("VelocityX", Mathf.Abs(rb.velocity.x));
            animatorPlayer.SetFloat("VelocityY", (rb.velocity.y));
        }
        
    }

    private void FixedUpdate()
    {
        
        MoveCharacter(); // Move the character based on the user input
        ApplyLinearDrag(); // Apply drag to the character's movement when not moving
        playerInput.CheckLeft(); // Check if the player is facing a wall on the left
        playerInput.CheckRight(); // Check if the player is facing a wall on the right
        playerInput.CheckRamp();
        Jump();
    }

    // Transforms the inputs to a vector2
    private Vector2 GetInput()
    {
        return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")); // Get the user input and return it as a vector2
    }

    // Moves the character based on the user input
    private void MoveCharacter()
    {
        // Adds a force to the character that gets stronger over time
        rb.AddForce(new Vector2(hDirection, 0f) * (movementAcceleration * multiplier));
        

        // Clamps the speed to the variable maxSpeed
        if (Mathf.Abs(rb.velocity.x) > (maxSpeed * multiplier))
        {
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * (maxSpeed * multiplier), rb.velocity.y);
        }
    }

    // Applies drag to the character's movement when not moving
    private void ApplyLinearDrag()
    {
        if (hDirection == 0 && playerInput.isGrounded)
        {
            for (int i = 0; i < 20 ;i++)
            {
                rb.drag = i * 0.1f; // Apply drag to the character's movement
            }
            
        }
        else { rb.drag = 0f; }

    }
    private void Jump()
    {
        if (playerInput.isGrounded == true && Input.GetButtonDown("Jump") && playerInput.isAlive)
        {
            animatorPlayer.SetTrigger("Jump");
            jump --;
            playerInput.isGrounded = false;

            rb.AddForce(new Vector2(0f, (jumpThrust * multiplier)), ForceMode2D.Impulse);
            UnityEngine.Debug.Log(new Vector2(rb.velocity.y,rb.velocity.y));
        }
    }
    public void KillPlayer()
    {
        myCheckpointManager.RespawnOnCheckpoint();
    }
}

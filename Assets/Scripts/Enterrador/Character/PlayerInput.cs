using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInput : MonoBehaviour
{
    public LayerMask groundLayer;
    public LayerMask rampLayer;
    public bool isGrounded;
    public bool isOnRamp;
    public CircleCollider2D detector;
    public float distance;
    public bool isOnWallLeft;
    public bool isOnWallRight;
    public float placeholderFloat;
    public bool sameAsGrounded;
    public PlayerMovement playerMovement;

    #region Life

    public bool isAlive;
    public byte lives;
    #endregion

    #region Animaciones
    public bool isAttacking;
    public bool isJumping;
    public float isFalling;
    public float distanceToGround;
    public bool isWalking;

    #endregion
    // Update is called once per frame

    private void Start()
    {
        lives = 3;
    }
    private void FixedUpdate()
    {
        if (isAlive)
        {
            CheckGround();
        }
        

    }


    public void CheckGround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, distanceToGround, groundLayer);
           if (hit.collider != null)
            {
                // The character is grounded
                isGrounded = true;
            }
            else
            {
                // The character is in the air
                isGrounded = false;
            }



    }
    public void Damage()
    {
        if (lives > 0)
        {
            lives--;
            playerMovement.animatorPlayer.SetTrigger("Ouch");
        }
        if (lives == 0)
        {
            isAlive = false;
            while (true)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
    }
    public void CheckRamp()
    {
        //    RaycastHit2D hitramp = Physics2D.Raycast(transform.position, Vector2.down, 0.40f, rampLayer);
        //    if (hitramp.collider != null)
        //    {
        //        // The character is grounded
        //        isOnRamp = true;
        //    }
        //    else
        //    {
        //        // The character is in the air
        //        isOnRamp = false;
        //    }

    }
    public void CheckRight()
    {
        RaycastHit2D hitwall = Physics2D.Raycast(transform.position + Vector3.right * 0.01f, Vector2.right, distanceToGround, groundLayer);
        if (hitwall.collider != null)
        {
            // The character is against a wall
            isOnWallRight = true;
        }
        else
        {
            // The character is not against a wall
            isOnWallRight = false;
        }
    }
    public void CheckLeft()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.left * 0.01f, Vector2.left, distanceToGround, groundLayer);
        if (hit.collider != null)
        {
            // The character is against a wall
            isOnWallLeft = true;
        }
        else
        {
            // The character is not against a wall
            isOnWallLeft = false;
        }
    }
    public void CheckCeilling()
    {
        {
            Vector2 boxSize = new Vector2(0.64f, 0.64f);
            Collider2D[] colliders = Physics2D.OverlapBoxAll(transform.position, boxSize, 0f, groundLayer);

            if (colliders.Length > 0)
            {
                Debug.Log("Object detected above or inside the character!");
                // Do something with the detected object
            }
        }
    }


}